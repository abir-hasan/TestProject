package me.abir.testproject.presenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import me.abir.testproject.bus_model.LoadNowPlaying;
import me.abir.testproject.bus_model.LoadTopMovies;
import me.abir.testproject.bus_model.LoadUpcoming;
import me.abir.testproject.interfaces.MoviesContract;
import me.abir.testproject.model.NowPlayingModel;
import me.abir.testproject.model.TopRatedMoviesModel;
import me.abir.testproject.model.UpcomingMoviesModel;
import me.abir.testproject.network.MovieDbApi;
import me.abir.testproject.network.RetrofitClient;
import me.abir.testproject.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Abir on 19-May-18.
 */
public class MoviesPresenter implements MoviesContract.Presenter {
    private MoviesContract.View view;
    private MovieDbApi endPoints;
    private EventBus eventBus = EventBus.getDefault();
    private int topMoviePage = 1;
    private int nowPlayPage = 1;
    private int upcomingPage = 1;
    private boolean loadingTopMovie;
    private boolean loadingNowPlaying;
    private boolean loaddingUpcoming;

    public MoviesPresenter(MoviesContract.View view) {
        this.view = view;
        endPoints = RetrofitClient.getInstance().getRetrofitClient().create(MovieDbApi.class);
        registerEventBus();
    }

    private void loadUpComingMovies() {
        endPoints.getMoviesUpComing(Constants.API_KEY, Constants.LANGUAGE_TAG, upcomingPage)
                .enqueue(new Callback<UpcomingMoviesModel>() {
                    @Override
                    public void onResponse(Call<UpcomingMoviesModel> call,
                                           Response<UpcomingMoviesModel> response) {
                        if (response.isSuccessful() && view != null) {
                            view.onUpComingMovies(response.body() != null ?
                                    response.body().getResults() : null);
                        }
                        loaddingUpcoming = false;
                    }

                    @Override
                    public void onFailure(Call<UpcomingMoviesModel> call, Throwable t) {
                        loaddingUpcoming = false;
                    }
                });
        loaddingUpcoming = true;
        upcomingPage++;
    }

    private void loadNowPlayingMovies() {
        endPoints.getMoviesNowPlaying(Constants.API_KEY, Constants.LANGUAGE_TAG, nowPlayPage)
                .enqueue(new Callback<NowPlayingModel>() {
                    @Override
                    public void onResponse(Call<NowPlayingModel> call,
                                           Response<NowPlayingModel> response) {
                        if (response.isSuccessful() && view != null) {
                            view.onNowPlayingMovies(response.body() != null ?
                                    response.body().getResults() : null);
                        }
                        loadingNowPlaying = false;
                    }

                    @Override
                    public void onFailure(Call<NowPlayingModel> call, Throwable t) {
                        loadingNowPlaying = false;
                    }
                });

        loadingNowPlaying = true;
        nowPlayPage++;
    }

    private void loadTopRatedMovies() {
        endPoints.getTopRatedMovies(Constants.API_KEY, Constants.LANGUAGE_TAG, topMoviePage)
                .enqueue(new Callback<TopRatedMoviesModel>() {
                    @Override
                    public void onResponse(Call<TopRatedMoviesModel> call, Response<TopRatedMoviesModel>
                            response) {
                        if (response.isSuccessful() && view != null) {
                            view.onTopMovieResponse(response.body() != null ?
                                    response.body().getResults() : null);
                        }
                        loadingTopMovie = false;
                    }

                    @Override
                    public void onFailure(Call<TopRatedMoviesModel> call, Throwable t) {
                        loadingTopMovie = false;
                    }
                });
        loadingTopMovie = true;
        topMoviePage++;
    }

    @Override
    public void unSubscribe() {
        view = null;
        unregisterEventBus();
    }


    private void registerEventBus() {
        eventBus.register(this);
    }

    private void unregisterEventBus() {
        if (eventBus != null && eventBus.isRegistered(this))
            eventBus.unregister(this);
    }

    @Override
    public void loadMovies() {
        loadTopRatedMovies();
        loadNowPlayingMovies();
        loadUpComingMovies();
    }

    @Subscribe
    public void onLoadTopMovies(LoadTopMovies bus) {
        if (!loadingTopMovie)
            loadTopRatedMovies();
    }

    @Subscribe
    public void onLoadUpcoming(LoadUpcoming bus) {
        if (!loaddingUpcoming)
            loadUpComingMovies();
    }

    @Subscribe
    public void onLoadNowPlaying(LoadNowPlaying bus) {
        if (!loadingNowPlaying)
            loadNowPlayingMovies();
    }
}
