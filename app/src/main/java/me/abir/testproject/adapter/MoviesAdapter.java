package me.abir.testproject.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.abir.testproject.R;
import me.abir.testproject.model.Result;
import me.abir.testproject.utils.Constants;
import me.abir.testproject.view_holder.MoviesViewHolder;

/**
 * Created by Abir on 19-May-18.
 */
public class MoviesAdapter extends RecyclerView.Adapter {
    private List<Result> topMovies = new ArrayList<>();
    private List<Result> upcomingMovies = new ArrayList<>();
    private List<Result> nowPlayingMovies = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_movie_list,
                parent, false);
        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (position) {
            case 0:
                ((MoviesViewHolder) holder).setMoviesByType(topMovies, Constants.TYPE_TOP_MOVIES);
                break;
            case 1:
                ((MoviesViewHolder) holder).setMoviesByType(upcomingMovies,
                        Constants.TYPE_UPCOMING_MOVIES);
                break;
            case 2:
                ((MoviesViewHolder) holder).setMoviesByType(nowPlayingMovies,
                        Constants.TYPE_NOW_PLAYING_MOVIES);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public void setTopMovieData(List<Result> topMovies) {
        this.topMovies.addAll(topMovies);
        notifyItemChanged(0);
    }

    public void setUpcomingMovies(List<Result> topMovies) {
        this.upcomingMovies.addAll(topMovies);
        notifyItemChanged(1);
    }

    public void setNowPlayingMovies(List<Result> topMovies) {
        this.nowPlayingMovies.addAll(topMovies);
        notifyItemChanged(2);
    }
}
