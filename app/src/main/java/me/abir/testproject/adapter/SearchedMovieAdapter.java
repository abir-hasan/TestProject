package me.abir.testproject.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.abir.testproject.R;
import me.abir.testproject.view_holder.MoviesViewHolder;
import me.abir.testproject.view_holder.SearchedVH;

/**
 * Created by Abir on 19-May-18.
 */
public class SearchedMovieAdapter extends RecyclerView.Adapter {

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_searched_movie_row,
                parent, false);
        return new SearchedVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }
}
