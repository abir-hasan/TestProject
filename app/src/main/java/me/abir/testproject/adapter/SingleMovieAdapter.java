package me.abir.testproject.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.abir.testproject.R;
import me.abir.testproject.model.Result;
import me.abir.testproject.view_holder.SingleMovieViewHolder;

/**
 * Created by Abir on 19-May-18.
 */
public class SingleMovieAdapter extends RecyclerView.Adapter {
    private List<Result> movies = new ArrayList<>();

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_single_movie,
                parent, false);
        return new SingleMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((SingleMovieViewHolder) holder).setSingleMovieData(movies.get(position));
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setMovies(List<Result> results) {
        this.movies.clear();
        this.movies.addAll(results);
        notifyDataSetChanged();
    }
}
