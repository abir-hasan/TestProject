package me.abir.testproject.utils;

/**
 * Created by Abir on 19-May-18.
 */
public class Constants {
    public static final String API_KEY = "4bc53930f5725a4838bf943d02af6aa9";
    public static final String LANGUAGE_TAG = "en-US";
    public static final String TYPE_TOP_MOVIES = "Top Movies";
    public static final String TYPE_NOW_PLAYING_MOVIES = "Now Playing";
    public static final String TYPE_UPCOMING_MOVIES = "Up Coming";
}
