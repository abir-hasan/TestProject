package me.abir.testproject.interfaces;

import java.util.List;

import me.abir.testproject.model.Result;

/**
 * Created by Abir on 19-May-18.
 */
public interface MoviesContract {

    interface View {

        void onErrorResponse();

        void onTopMovieResponse(List<Result> results);

        void onNowPlayingMovies(List<Result> results);

        void onUpComingMovies(List<Result> results);
    }

    interface Presenter {

        void loadMovies();

        void unSubscribe();
    }
}
