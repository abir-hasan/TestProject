package me.abir.testproject.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import me.abir.testproject.R;
import me.abir.testproject.adapter.MoviesAdapter;
import me.abir.testproject.interfaces.MoviesContract;
import me.abir.testproject.model.Result;
import me.abir.testproject.presenter.MoviesPresenter;

/**
 * Created by Abir on 19-May-18.
 */
public class MoviesFragments extends Fragment implements MoviesContract.View {

    private Context context;
    private RecyclerView rvMovies;
    private MoviesAdapter adapter;
    private MoviesPresenter presenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.presenter = new MoviesPresenter(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
        if (presenter != null)
            presenter.unSubscribe();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movies, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
        getMovies();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getMovies() {
        if (presenter != null)
            presenter.loadMovies();
    }

    private void initUI(View view) {
        rvMovies = view.findViewById(R.id.rvMovies);
        rvMovies.setLayoutManager(new LinearLayoutManager(context));
        adapter = new MoviesAdapter();
        rvMovies.setAdapter(adapter);
    }


    @Override
    public void onErrorResponse() {

    }

    @Override
    public void onTopMovieResponse(List<Result> results) {
        if (adapter != null && results != null)
            adapter.setTopMovieData(results);
    }

    @Override
    public void onNowPlayingMovies(List<Result> results) {
        if (adapter != null && results != null)
            adapter.setNowPlayingMovies(results);
    }

    @Override
    public void onUpComingMovies(List<Result> results) {
        if (adapter != null && results != null)
            adapter.setUpcomingMovies(results);
    }
}
