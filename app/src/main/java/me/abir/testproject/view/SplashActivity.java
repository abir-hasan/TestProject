package me.abir.testproject.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import me.abir.testproject.R;

/**
 * Created by Abir on 19-May-18.
 */
public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";
    public static final int SPLASH_DELAY = 2000;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setSplashDelay();
    }

    private void setSplashDelay() {
        Log.d(TAG, "setSplashDelay() start");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setLandingScreen();
                Log.i(TAG, "setSplashDelay() Delay done");
            }
        }, SPLASH_DELAY);
        Log.d(TAG, "setSplashDelay() end");
    }

    private void setLandingScreen() {
        Intent intent = new Intent(this, MoviesActivity.class);
        startActivity(intent);
        finish();
    }
}
