package me.abir.testproject.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.abir.testproject.R;
import me.abir.testproject.adapter.SearchedMovieAdapter;

/**
 * Created by Abir on 19-May-18.
 */
public class SearchMovieFragment extends Fragment {

    private RecyclerView rvSearchedMovies;
    private SearchedMovieAdapter adapter;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_movie, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        rvSearchedMovies = view.findViewById(R.id.rvSearchedMovies);
        rvSearchedMovies.setLayoutManager(new LinearLayoutManager(context));
        adapter = new SearchedMovieAdapter();
        rvSearchedMovies.setAdapter(adapter);
    }
}
