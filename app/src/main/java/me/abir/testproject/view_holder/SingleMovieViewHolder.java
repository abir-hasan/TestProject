package me.abir.testproject.view_holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import me.abir.testproject.R;
import me.abir.testproject.model.Result;
import me.abir.testproject.network.MovieDbApi;

/**
 * Created by Abir on 19-May-18.
 */
public class SingleMovieViewHolder extends RecyclerView.ViewHolder {

    private ImageView ivMovie;
    private TextView tvMovieName;
    private TextView tvMovieYear;

    public SingleMovieViewHolder(View itemView) {
        super(itemView);
        ivMovie = itemView.findViewById(R.id.ivMovie);
        tvMovieName = itemView.findViewById(R.id.tvMovieName);
        tvMovieYear = itemView.findViewById(R.id.tvMovieYear);
    }

    public void setSingleMovieData(Result result) {
        String date = result.getReleaseDate().substring(0, 4);
        String url = MovieDbApi.IMAGE_BASE_URL + result.getPosterPath();

        tvMovieName.setText(result.getTitle());
        tvMovieYear.setText(date);
        Picasso.with(ivMovie.getContext()).load(url)
                .fit()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.drawable.ic_launcher_foreground)
                .into(ivMovie);

    }
}
