package me.abir.testproject.view_holder;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import me.abir.testproject.R;
import me.abir.testproject.adapter.SingleMovieAdapter;
import me.abir.testproject.bus_model.LoadNowPlaying;
import me.abir.testproject.bus_model.LoadTopMovies;
import me.abir.testproject.bus_model.LoadUpcoming;
import me.abir.testproject.model.Result;
import me.abir.testproject.utils.Constants;

/**
 * Created by Abir on 19-May-18.
 */
public class MoviesViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = "MoviesViewHolder";
    private RecyclerView rvMoviesByType;
    private TextView tvMovieListType;
    private SingleMovieAdapter adapter;
    private ProgressBar progressBar;
    private int visibleThreshold = 1;
    private String type = "";

    public MoviesViewHolder(View itemView) {
        super(itemView);
        progressBar = itemView.findViewById(R.id.progressBar);
        tvMovieListType = itemView.findViewById(R.id.tvMovieListType);
        rvMoviesByType = itemView.findViewById(R.id.rvMoviesByType);
        rvMoviesByType.setLayoutManager(new LinearLayoutManager(itemView.getContext(),
                LinearLayoutManager.HORIZONTAL, false));
        adapter = new SingleMovieAdapter();
        rvMoviesByType.setAdapter(adapter);
        setScroller();
    }

    public void setMoviesByType(List<Result> moviesByType, String type) {
        this.type = type;
        tvMovieListType.setText(type);
        adapter.setMovies(moviesByType);
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void setScroller() {
        rvMoviesByType.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();


                int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager())
                        .findLastVisibleItemPosition();


                if (lastVisibleItemPosition >= (totalItemCount - visibleThreshold)) {
                    Log.w(TAG, "onScrolled() called with: recyclerView = ["
                            + recyclerView + "], dx = [" + dx + "], dy = [" + dy + "]");
                    progressBar.setVisibility(View.VISIBLE);
                    switch (type) {
                        case Constants.TYPE_TOP_MOVIES:
                            EventBus.getDefault().post(new LoadTopMovies());
                            break;
                        case Constants.TYPE_UPCOMING_MOVIES:
                            EventBus.getDefault().post(new LoadUpcoming());
                            break;
                        case Constants.TYPE_NOW_PLAYING_MOVIES:
                            EventBus.getDefault().post(new LoadNowPlaying());
                            break;
                    }
                }
            }
        });
    }

}
