package me.abir.testproject.network;

import me.abir.testproject.model.NowPlayingModel;
import me.abir.testproject.model.TopRatedMoviesModel;
import me.abir.testproject.model.UpcomingMoviesModel;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Abir on 19-May-18.
 */
public interface MovieDbApi {

    String BASE_URL = "https://api.themoviedb.org/3/";

    String IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w185";

    @GET("movie/top_rated")
    Call<TopRatedMoviesModel> getTopRatedMovies(@Query("api_key") String apiKey,
                                                @Query("language") String language,
                                                @Query("page") int page);

    @GET("movie/now_playing")
    Call<NowPlayingModel> getMoviesNowPlaying(@Query("api_key") String apiKey,
                                              @Query("language") String language,
                                              @Query("page") int page);

    @GET("movie/upcoming")
    Call<UpcomingMoviesModel> getMoviesUpComing(@Query("api_key") String apiKey,
                                                @Query("language") String language,
                                                @Query("page") int page);

    @GET("search/movie")
    Call<Response> getSearchedMovies(@Query("api_key") String apiKey,
                                     @Query("language") String language,
                                     @Query("page") String query);
}
