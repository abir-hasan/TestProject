package me.abir.testproject.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Abir on 19-May-18.
 */
public class RetrofitClient {

    private static RetrofitClient instance;

    private static Retrofit retrofit;

    private RetrofitClient() {

    }

    public static RetrofitClient getInstance() {
        if (instance == null) {
            instance = new RetrofitClient();

            retrofit = new Retrofit.Builder()
                    .baseUrl(MovieDbApi.BASE_URL)
                    .client(getOkClient())
                    .addConverterFactory(GsonConverterFactory.create(getGsonClient()))
                    .build();
        }
        return instance;
    }

    public  Retrofit getRetrofitClient() {
        return retrofit;
    }

    private static Gson getGsonClient() {
        GsonBuilder builder = new GsonBuilder();
        // Need any deserializer; add them here to the type adapter
        return builder.create();
    }

    private static OkHttpClient getOkClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }
}
